$(document).ready(function(){
    const btnStyle = {
        width:'100px',
        height:'100px',
        backgroundColor: 'black',
        color: 'white',
        position: 'fixed',
        right: '50px',
        bottom: '50px',
        borderRadius: '100%',
        border: 'none',
        boxShadow: '0 0 10px rgba(0,0,0,0.5)',
        outline: 'none'
    };

    const $btnUp = $('<button id="back">Наверх</button>');
    const $btnToggle = $('<button id="toggle">toggle</button>');

    $btnToggle.insertAfter('.posts').addClass('news__button-action').css({backgroundColor: '#89ca62'});

    $btnToggle.on('click', function(){
        $('.posts').slideToggle(2000);
    });


    $btnUp.css(btnStyle).on('click', function(){
        $(document.documentElement).animate({
            scrollTop: 0
        }, 2000);
    });

    $('.menu__nav').on('click', function(event){
        let dataId = $(event.target).data('id');
        $(document.documentElement).animate({
            scrollTop: $(`section[data-id="${dataId}"]`).offset().top
        }, 2000);
    });

    $(window).scroll(function(){
        const height = $(window).height();
        const scrollTop = $(window).scrollTop();


        if(scrollTop > height) {
            $btnUp.appendTo(document.body);
        } else {
            $('#back').detach();
        }

    });



});





